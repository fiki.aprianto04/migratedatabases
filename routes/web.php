<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/table', function (){
    return view('halaman.table');
});

Route::get('/data-table', function (){
    return view('halaman.data-table');
});

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/', 'IndexController@halamanutama');
Route::get('/pendaftaran', 'FormController@regis');

Route::post('/submit', 'FormController@welcome');

//crud
///form create data
Route::get('/cast/create', 'CastController@create');
//input to cast table
Route::post('/cast', 'CastController@store');
//sow data cast
Route::get('cast', 'CastController@index');
//detail
Route::get('/cast/{cast_id}', 'CastController@show');
//edit data
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//update data table
Route::put('/cast/{cast_id}', 'CastController@update');
//delet
Route::delete('/cast/{cast_id}', 'CastController@destroy');