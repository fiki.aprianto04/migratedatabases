<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB; 

class castcontroller extends Controller
{
    public function create()
{
    return view('cast.create');
}

//input tb cast
public function store(Request $request){
    //dd($request->all());
    $request->validate(
[
    'nama' => 'required',
    'umur' => 'required',
    'bio' => 'required',
],
[
    'nama.required' => 'harus diisi',
    'umur.required' => 'harus diisi',
    'bio.required' => 'harus diisi',
]
 );

 DB::table('cast')->insert(
    [
        'nama' => $request['nama'],
        'umur' => $request['umur'],
        'bio' => $request['bio']
    ]
);
    return redirect('/cast');
}

//show cast
public function index()
{
    $cast = DB::table('cast')->get();

    return view('cast.index', compact('cast'));
}

public function show($id)
{
$cast = DB::table('cast')->where('id', $id)->first();

return view('cast.show', compact('cast'));

}

public function edit($id)
{
$cast = DB::table('cast')->where('id', $id)->first();

return view('cast.edit', compact('cast'));

}

public function update($id, Request $request)
{
    $request->validate(
        [
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'harus diisi',
            'umur.required' => 'harus diisi',
            'bio.required' => 'harus diisi',
        ]

        );

        DB::table('cast')->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']  
                ]
                );
                return redirect('cast');
}
        public function destroy($id)
        {
            DB::table('cast')->where('id', '=', $id)->delete();

            return redirect('/cast');
        }


}
